import { CELL_MARGIN, NUMBER_OF_COLUMNS, NUMBER_OF_ROWS } from "../const";
import { viewEvents } from "../Controller/Controller";
import { BaseView } from "./BaseView";
import { GridView } from "./GridView";


export class TokenView extends BaseView {


    private _gridElement: HTMLElement;
    private _gridView: GridView;
    private _tokenElement: HTMLElement;
    private _row: number;
    private _column: number;
    private _player: number;

    fallDuration: number;

    constructor(gridView: GridView, column: number, line: number, player: number) {
        super();
        this.gridView = gridView;
        this.gridElement = gridView.gridElement;
        this.initValues(column, line, player);
    }

    private initValues(column: number, row: number, player: number) {
        this.row = row;
        this.column = column;
        this.player = player;
        const speed = 20;
        const distance = this.row + 1;
        this.fallDuration = distance / speed;

        this.tokenElement = this.createElement("div", ["token", "neon"]);
        this.setStyle(column);

        this.addAnimations();
        this.gridElement.appendChild(this.tokenElement);

    }

    private addAnimations() {
        this.setCssVar("target-height", (2.2 + 10 * this.row) + "vh");
        this.tokenElement.style.animation = "fall " + this.fallDuration + "s forwards";
        this.tokenElement.style.animationTimingFunction = "linear";
        this.tokenElement.addEventListener("animationstart", this.onAnimationStart.bind(this), true);
        this.tokenElement.addEventListener("animationend", this.onAnimationEnd.bind(this), true);
    }

    private onAnimationStart(event: AnimationEvent): void {
        if (event.animationName === "tmp-flicker") {
            return;
        }
        this.gridView.canPlay = false;
        this.gridView.startHidingCell(this.column, this.row, this.fallDuration);
    }

    private onAnimationEnd(event: AnimationEvent): void {
        if (event.animationName === "tmp-flicker") {
            this.gridView.canPlay = true;
        }
        if (event.animationName === "fall") {
            const vitesse = 6;
            const distance = this.row + 1;
            let duration = distance / vitesse;
            this.tokenElement.style.top = (2.2 + this.row * 10) + "vh";
            this.tokenElement.style.animation = "";
            if (this.row !== 0) {
                this.tokenElement.classList.add("rebound");
                this.setCssVar("rebound-animation-duration", duration + "s");
            } else {
                this.gridView.canPlay = true;
                this.tokenElement.style.animation = "none";
                viewEvents.emit('nextTurn');
            }
        }

        if (event.animationName === "rebound") {
            this.gridView.canPlay = true;
            this.gridView.hideCell(this.column, this.row, this.fallDuration);
            this.tokenElement.classList.remove("rebound");
            viewEvents.emit('nextTurn');
        }
    }

    private setStyle(column: number) {
        this.setSize();
        this.setBasePosition(column);
        this.updateColor();
    }

    private setSize() {
        const tokenSize = this.getTokenSize();
        this.tokenElement.style.width = tokenSize.width;
        this.tokenElement.style.height = tokenSize.height;
    }

    private setBasePosition(column: number) {
        this.tokenElement.style.left = (2.3 + column * 10) + "vh";
        this.tokenElement.style.top = -12.2 + "vh";
    }

    public getColor(): string {
        if (this.player == 1) {
            return this.cssVar('player1-color');
        } else {
            return this.cssVar('player2-color');
        }
    }

    public updateColor() {
        this.tokenElement.style.color = this.getColor();
    }

    //Setters and getters

    get gridElement(): HTMLElement {
        return this._gridElement
    }
    get gridView(): GridView {
        return this._gridView
    }
    get tokenElement(): HTMLElement {
        return this._tokenElement
    }
    get row(): number {
        return this._row
    }
    get column(): number {
        return this._column
    }
    get player(): number {
        return this._player
    }

    set gridElement(_gridElement: HTMLElement) {
        this._gridElement = _gridElement;
    }
    set gridView(_gridView: GridView) {
        this._gridView = _gridView;
    }
    set tokenElement(_tokenElement: HTMLElement) {
        this._tokenElement = _tokenElement;
    }
    set row(_row: number) {
        this._row = _row;
    }
    set column(_column: number) {
        this._column = _column;
    }
    set player(_player: number) {
        this._player = _player;
    }

}