import { NUMBER_OF_CELLS, NUMBER_OF_COLUMNS, NUMBER_OF_ROWS } from "../const";
import { viewEvents } from "../Controller/Controller";
import { BaseView } from "./BaseView";
import { TokenView } from "./TokenView";
import { VictoryView } from "./VictoryView";


export class GridView extends BaseView {

    private _gridElement: HTMLElement;
    private _cells: HTMLElement[];
    private _arrowElement: HTMLElement;
    private _tokens: TokenView[];
    private _currentPlayer: number;
    private _canPlay: boolean;
    private _bodyElement: HTMLElement;
    private _canPreview: boolean;

    constructor() {
        super();
        this.initValues();
    }


    private initValues() {
        this.setCssVar("number-of-columns", String(NUMBER_OF_COLUMNS));
        this.setCssVar("number-of-lines", String(NUMBER_OF_ROWS));
        this.canPreview = false;
        this.canPlay = false;
        this.gridElement = this.createElement("div", ["grid", "neon-box", "ignition"]);
        this.gridElement.addEventListener("animationend", (ev: AnimationEvent) => {
            if (ev.animationName === "ignition-flicker") {
                this.gridElement.classList.remove("ignition");
            }
        })
        this.arrowElement = this.createElement("div", ["arrow-icon"]);
        this.arrowElement.innerHTML = 'V';
        this.currentPlayer = 1;
        this.gridElement.appendChild(this.arrowElement);
        this.createCells();
        this.app.prepend(this.gridElement);
        this.tokens = [];


        this.initBackgroundColor();
        this.bindClick();
        this.bindPreview();

        const shutdownDuration = parseFloat(this.cssVar('shutdown-duration').slice(0, -1));
        setTimeout(() => {
            this.canPlay = true;
            this.canPreview = true;
            viewEvents.emit("canQuickRestart");
            viewEvents.emit("start");
        }, shutdownDuration * 1000);
    }

    public reset() {
        this.gridElement.innerHTML = "";
        this.app.removeChild(this.gridElement);
        this.initValues();
    }

    async showMessage(message: string) {
        window.alert(message);
    }


    public gameOver(winningTokens: { column: number, row: number }[], isWinnerAI: boolean) {
        this.canPreview = false;
        this.canPlay = false;
        this.cells.forEach((cell: HTMLElement) => {
            cell.classList.remove("preview");
        })
        let winner: number;
        if (winningTokens.length === 0) {
            winner = -1;
        } else {
            winner = this.getToken(winningTokens[0].column, winningTokens[0].row).player;
        }
        winningTokens.forEach((winningToken: { column: number, row: number }) => {
            const token = this.getToken(winningToken.column, winningToken.row);
            if (token) {
                token.tokenElement.classList.toggle("highlight");
            }
        });

        setTimeout(() => {
            this.shutdown();
        }, 1000)

        const shutdownDuration = parseFloat(this.cssVar('shutdown-duration').slice(0, -1));
        setTimeout(() => {
            this.gridElement.innerHTML = "";
            new VictoryView(winner, isWinnerAI, this);
        }, 2500 + (shutdownDuration * 1000));
    }

    public shutdown() {
        this.gridElement.classList.toggle("shutdown");
        this.tokens.forEach((token: TokenView) => {
            if (!token.tokenElement.classList.contains("highlight")) {
                token.tokenElement.addEventListener("animationend", (ev: AnimationEvent) => {
                    if (ev.animationName === "shutdown-flicker") {
                        this.gridElement.removeChild(token.tokenElement);
                    }
                });
            }
        });
        
        const shutdownDuration = parseFloat(this.cssVar('shutdown-duration').slice(0, -1));
        setTimeout(() => {
            this.tokens.forEach((token: TokenView) => {
                if (token.tokenElement.classList.contains("highlight")) {
                    token.tokenElement.classList.add("fall");
                    token.tokenElement.style.transition = "top 1.5s ease-in-out";
                    let topString: string = token.tokenElement.style.top;
                    let newTop: number = parseFloat(topString.slice(0, -2)) + 100;
                    token.tokenElement.style.top = newTop + "vh";
                }
            });
        }, shutdownDuration * 1000);
    }

    public getToken(column, row): TokenView | null {
        return this.tokens.find((token: TokenView) => token.column === column && token.row === row);
    }

    public getPlayableCell(column: number): HTMLElement | null {
        let cell: HTMLElement | null = null;
        let maxIndex = -1;
        this.cells.forEach((c: HTMLElement, index) => {
            if (index % NUMBER_OF_COLUMNS === column && index > maxIndex && !c.classList.contains("hidden")) {
                maxIndex = index;
            }
        })
        return maxIndex === -1 ? null : this.cells[maxIndex];
    }


    private initBackgroundColor() {

        const mainColor = this.cssVar('main-color');
        const rgbArray = this.hexToRgb(mainColor);
        document.body.style.backgroundColor = "rgb(" + Math.floor(rgbArray.r / 9) + "," + Math.floor(rgbArray.g / 9) + "," + Math.floor(rgbArray.b / 9) + ")";
    }

    private createCells() {
        this.cells = [];
        for (let i = 0; i < NUMBER_OF_CELLS; i++) {
            const newCell: HTMLElement = this.createElement("div", ["cell", "neon"]);
            newCell.addEventListener("mouseover", () => this.changeArrowPosition(i % 7));
            newCell.addEventListener("animationend", (ev: AnimationEvent) => {
                if (ev.animationName === "ignition-flicker") {
                    newCell.style.animation = "none";
                }
            })
            this.cells.push(newCell);
            this.gridElement.appendChild(newCell);
        }
    }

    private addPreview(column: number, row: number) {
        if (!this.canPreview) return;
        if (row >= 0) {
            const indexToToggle = (row) * 7 + column;
            this.cells[indexToToggle].classList.add("preview");
        }

    }

    private removePreview(column: number, row: number) {
        if (row >= 0) {
            const indexToToggle = (row) * 7 + column;
            this.cells[indexToToggle].classList.remove("preview");
        }

    }

    public handlePlayerColorChange() {
        this.tokens.forEach((token: TokenView) => {
            token.updateColor();
        })
    }

    public playAnimation(column: number, row: number) {
        if (!this.canPlay || row < 0) {
            return;
        }
        const newToken: TokenView = new TokenView(this, column, row, this.currentPlayer);
        this.currentPlayer = Math.abs(this.currentPlayer - 1);
        this.tokens.push(newToken);
        //this.addPreview(column, row - 1);
    }

    private changeArrowPosition(column: number) {
        this.arrowElement.style.left = (3.5 + column * 10) + "vh";
    }

    public startHidingCell(column: number, row: number, duration: string | number) {
        this.cells.forEach((cell: HTMLElement, index: number) => {
            if (index % 7 === column && (index - index % 7) / 7 === row) {
                cell.classList.remove("preview");
                this.setCssVar('animation-duration', duration + "s");
                cell.classList.add("shutdown-flicker");
            }
        })
    }

    public hideCell(column: number, row: number, duration: string | number) {
        this.cells.forEach((cell: HTMLElement, index: number) => {
            if (index % 7 === column && (index - index % 7) / 7 === row) {
                cell.classList.toggle("hidden");
            }
        })
    }


    //Model-View functions

    public bindPreview() {
        this.cells.forEach((cell: HTMLElement, index: number) => {
            cell.addEventListener("mouseenter", () => {
                const column = index % 7;
                viewEvents.once("preview", this.addPreview.bind(this, column));
                viewEvents.emit("getPreview", column);
            });
            cell.addEventListener("mouseleave", () => {
                const column = index % 7;
                viewEvents.once("preview", this.removePreview.bind(this, column));
                viewEvents.emit("getPreview", column);
            });
        })
    }

    public bindClick() {
        this.cells.forEach((cell: HTMLElement, index: number) => {
            cell.addEventListener("click", () => {
                if (!this.canPlay) return;
                this.removeHints();
                const column = index % 7;
                viewEvents.emit("click", column);
            });
        });
    }

    public hint(move: number): void {
        const cell = this.getPlayableCell(move);
        if (cell) {
            cell.classList.add("hint");
            setTimeout(() => {
                cell.classList.remove("hint");
            }, 3000);
        }
    }

    public removeHints(): void {
        this.cells.forEach((cell: HTMLElement) => {
            if (cell.classList.contains("hint")) {
                cell.classList.remove("hint");
            }
        })
    }

    //#region Getters and setters


    get gridElement(): HTMLElement {
        return this._gridElement;
    }
    get cells(): HTMLElement[] {
        return this._cells;
    }
    get arrowElement(): HTMLElement {
        return this._arrowElement;
    }
    get tokens(): TokenView[] {
        return this._tokens;
    }
    get currentPlayer(): number {
        return this._currentPlayer;
    }
    get canPlay(): boolean {
        return this._canPlay;
    }
    get bodyElement(): HTMLElement {
        return this._bodyElement;
    }

    get canPreview(): boolean {
        return this._canPreview;
    }

    set gridElement(_gridElement: HTMLElement) {
        this._gridElement = _gridElement;
    }
    set cells(_cells: HTMLElement[]) {
        this._cells = _cells;
    }
    set arrowElement(_arrowElement: HTMLElement) {
        this._arrowElement = _arrowElement;
    }
    set tokens(_tokens: TokenView[]) {
        this._tokens = _tokens;
    }
    set currentPlayer(_currentPlayer: number) {
        this._currentPlayer = _currentPlayer;
    }
    set canPlay(_canPlay: boolean) {
        this._canPlay = _canPlay;
    }
    set bodyElement(_bodyElement: HTMLElement) {
        this._bodyElement = _bodyElement;
    }

    set canPreview(_canPreview: boolean) {
        this._canPreview = _canPreview;
    }

    //#endregion


}

