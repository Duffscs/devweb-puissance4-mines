import { GRID_DEFAULT_COLOR, PLAYER_1_DEFAULT_COLOR, PLAYER_2_DEFAULT_COLOR } from "../const";
import { viewEvents } from "../Controller/Controller";
import { BaseView } from "./BaseView";
import { GridView } from "./GridView";


export class UIView extends BaseView {

    private _mainPanelElement: HTMLElement;
    private _cursor: HTMLElement;
    private _player1ColorSelector: HTMLElement;
    private _player2ColorSelector: HTMLElement;
    private _cursorColorSelector: HTMLElement;
    private _gridColorSelector: HTMLElement;
    private _gridView: GridView;
    private _toggleAIElement: HTMLElement;
    private _AILevelElement: HTMLElement;
    private _toggleAIStartingElement: HTMLElement;
    private _keyboardBindingsElement: HTMLElement;
    private _canRestart: boolean;

    constructor(gridView: GridView) {
        super();
        this.initValues();
        this.gridView = gridView;
        viewEvents.on("canQuickRestart", () => { this.canRestart = true });
    }

    private initValues(): void {
        this.canRestart = false;
        this.cursor = document.querySelector("#cursor");
        this.addEventListeners();

        this.mainPanelElement = this.createElement("div", ["ui-main-panel", "neon-box", "ignition"]);
        this.mainPanelElement.addEventListener("animationend", (ev: AnimationEvent) => {
            if (ev.animationName === "ignition-flicker") {
                this.mainPanelElement.classList.remove("ignition");
            }
        })

        this.createAIMenu();

        this.keyboardBindingsElement = this.createElement("div", ["neon-text", "keyboard-bindings"]);
        this.keyboardBindingsElement.innerHTML = "<div>R : quick restart</div><div>H : next move help</div><div>A : toggle AI</div><div>S : toggle AI start</div><div>1-4 : AI level</div>"
        this.mainPanelElement.appendChild(this.keyboardBindingsElement);

        this.createColorPickers();

        this.app.appendChild(this.mainPanelElement);

        this.randomFlicker();

    }

    private addEventListeners() {
        window.addEventListener('mousemove', this.moveCursor)
        window.addEventListener("keyup", (ev: KeyboardEvent) => {
            this.onKeyUp(ev.key);
        })
    }

    private onKeyUp(key: string) {
        switch (key) {
            case "r":
            case "R":
                if (this.canRestart) {
                    viewEvents.emit("quickRestart");
                }
                break;
            case "a":
            case "A":
                viewEvents.emit("toggleAI");
                break;
            case "h":
            case "H":
                viewEvents.emit("helpPlayer");
                break;
            case "s":
            case "S":
                viewEvents.emit("toggleAIStart");
                break;
            case "&":
                viewEvents.emit("changeAILevel", 1);
                break;
            case "é":
                viewEvents.emit("changeAILevel", 2);
                break;
            case "\"":
                viewEvents.emit("changeAILevel", 3);
                break;
            case "'":
                viewEvents.emit("changeAILevel", 4);
                break;
        }
        if (parseInt(key) > 0 && parseInt(key) < 5) {
            viewEvents.emit("changeAILevel", parseInt(key));
        }
    }

    private randomFlicker() {
        const timeout = (this.getRandomInt(5) + 2) * 1000;
        const randint = this.getRandomInt(100);
        const querySelector = randint < 40 ? ".neon-box" : ".neon";
        const elements = document.querySelectorAll(querySelector);
        const randIndex = this.getRandomInt(elements.length);
        const randElement = elements[randIndex];
        if (randElement.classList.contains("tmp-flicker")) {
            this.randomFlicker();
            return;
        }
        randElement.classList.add("tmp-flicker");
        setTimeout(() => {
            randElement.classList.remove("tmp-flicker");
        }, timeout);
        setTimeout(() => {
            this.randomFlicker();

        }, timeout + 3000);
    }



    private createAIMenu() {
        const AIElementContainer = this.createElement("div", ["ai-element-container"]);
        this.toggleAIElement = this.createElement("div", ["neon-text", "ai-toggle", "hoverable"]);
        this.toggleAIElement.innerText = "AI";
        this.toggleAIElement.addEventListener("click", () => viewEvents.emit("toggleAI"))
        this.AILevelElement = this.createElement("div", ["ai-level-container"]);
        for (let i = 1; i < 5; i++) {
            const levelElement = this.createElement("div", ["ai-level", "neon-text", "AI-inactive"]);
            if (i == 1) levelElement.classList.add("active");
            levelElement.innerText = String(i);
            levelElement.addEventListener("click", () => {
                viewEvents.emit("changeAILevel", i);
            })
            this.AILevelElement.appendChild(levelElement);
        }
        this.toggleAIStartingElement = this.createElement("div", ["neon-text", "ai-starting", "hoverable", "AI-inactive"]);
        this.toggleAIStartingElement.innerText = "LET AI START";
        this.toggleAIStartingElement.addEventListener("click", () => { viewEvents.emit("toggleAIStart") })

        AIElementContainer.appendChild(this.toggleAIElement);
        AIElementContainer.appendChild(this.AILevelElement);
        AIElementContainer.appendChild(this.toggleAIStartingElement);
        this.mainPanelElement.appendChild(AIElementContainer);
    }

    private createColorPickers() {
        const colorPickerContainerElement = this.createElement("div", ["color-picker-container"]);
        this.player1ColorSelector = this.createCustomableInputElement("color", PLAYER_1_DEFAULT_COLOR, ["player-color-selector", "token", "player1", "ui-element", "hoverable"], []);
        this.player2ColorSelector = this.createCustomableInputElement("color", PLAYER_2_DEFAULT_COLOR, ["player-color-selector", "token", "player2", "ui-element", "hoverable"], []);
        this.gridColorSelector = this.createCustomableInputElement("color", GRID_DEFAULT_COLOR, ["grid-color-selector", "token", "hoverable", "ui-element"], []);
        this.cursorColorSelector = this.createCustomableInputElement("color", "#ffffff", ["cursor-color-picker"], []);
        const cursor = this.createElement("div", ["cursor"]);
        cursor.innerHTML = "v";
        this.cursorColorSelector.appendChild(cursor);

        const tokenSizeString = this.getTokenSize();
        this.player1ColorSelector.style.height = tokenSizeString.height;
        this.player1ColorSelector.style.width = tokenSizeString.width;
        this.player2ColorSelector.style.height = tokenSizeString.height;
        this.player2ColorSelector.style.width = tokenSizeString.width;
        this.gridColorSelector.style.height = tokenSizeString.height;
        this.gridColorSelector.style.width = tokenSizeString.width;

        this.player1ColorSelector.children[0].addEventListener("change", (e: any) => {
            this.setCssVar("player1-color", e.target.value);
            this.gridView.handlePlayerColorChange();
        });

        this.player2ColorSelector.children[0].addEventListener("change", (e: any) => {
            this.setCssVar("player2-color", e.target.value);
            this.gridView.handlePlayerColorChange();
        });

        this.gridColorSelector.children[0].addEventListener("change", (e: any) => {
            this.setCssVar("main-color", e.target.value);
            this.gridView.handlePlayerColorChange();
        });

        this.cursorColorSelector.children[0].addEventListener("change", (e: any) => {
            this.setCssVar("cursor-color", e.target.value);
            this.gridView.handlePlayerColorChange();
        });

        colorPickerContainerElement.appendChild(this.player1ColorSelector);
        colorPickerContainerElement.appendChild(this.player2ColorSelector);
        colorPickerContainerElement.appendChild(this.gridColorSelector);
        colorPickerContainerElement.appendChild(this.cursorColorSelector);

        const label = this.createElement("div", ["neon-text", "color-picker-label"]);
        label.innerText = "color pickers";

        colorPickerContainerElement.appendChild(label);
        this.mainPanelElement.appendChild(colorPickerContainerElement);
    }

    public toggleAI() {
        this.toggleAIElement.classList.toggle("active");
        Array.from(this.AILevelElement.children).forEach((child: HTMLElement, index: number) => {
            child.classList.toggle("AI-inactive");
        });
        this.toggleAIStartingElement.classList.toggle("AI-inactive");
    }

    public toggleAIStart() {
        this.toggleAIStartingElement.classList.toggle("active");
    }

    public setAIStart(active: boolean) {
        if (active) {
            this.toggleAIStartingElement.classList.add("active");
        } else {
            this.toggleAIStartingElement.classList.remove("active");
        }
    }

    public setAITrue() {
        if (!this.toggleAIElement.classList.contains("active")) {
            this.toggleAIElement.classList.toggle("active");
        }
        Array.from(this.AILevelElement.children).forEach((child: HTMLElement, index: number) => {
            child.classList.remove("AI-inactive");
        });
        this.toggleAIStartingElement.classList.remove("AI-inactive");
    }

    public changeAILevel(level: number) {
        Array.from(this.AILevelElement.children).forEach((child: HTMLElement, index: number) => {
            if (child.classList.contains("active")) {
                child.classList.remove("active");
            }
            if (index === level - 1) {
                child.classList.add("active");
            }
        });
    }

    private moveCursor(e) {
        const mouseY = e.clientY;
        const mouseX = e.clientX;

        this.cursor.style.transform = `translate3d(${mouseX}px, ${mouseY}px, 0) rotate(150deg)`;

        this.cursor.style.transform = `translate3d(${mouseX}px, ${mouseY}px, 0) rotate(150deg)`;

    }

    get mainPanelElement(): HTMLElement {
        return this._mainPanelElement;
    }

    set mainPanelElement(_mainPanelElement: HTMLElement) {
        this._mainPanelElement = _mainPanelElement;
    }

    get canRestart(): boolean {
        return this._canRestart;
    }

    set canRestart(_canRestart: boolean) {
        this._canRestart = _canRestart;
    }

    get cursor(): HTMLElement {
        return this._cursor;
    }

    set cursor(_cursor: HTMLElement) {
        this._cursor = _cursor;
    }

    get player1ColorSelector(): HTMLElement {
        return this._player1ColorSelector;
    }

    set player1ColorSelector(_player1ColorSelector: HTMLElement) {
        this._player1ColorSelector = _player1ColorSelector;
    }

    get player2ColorSelector(): HTMLElement {
        return this._player2ColorSelector;
    }

    set player2ColorSelector(_player2ColorSelector: HTMLElement) {
        this._player2ColorSelector = _player2ColorSelector;
    }

    get cursorColorSelector(): HTMLElement {
        return this._cursorColorSelector;
    }

    set cursorColorSelector(_cursorColorSelector: HTMLElement) {
        this._cursorColorSelector = _cursorColorSelector;
    }

    get gridColorSelector(): HTMLElement {
        return this._gridColorSelector;
    }

    set gridColorSelector(_gridColorSelector: HTMLElement) {
        this._gridColorSelector = _gridColorSelector;
    }

    get gridView(): GridView {
        return this._gridView;
    }

    set gridView(_gridView: GridView) {
        this._gridView = _gridView;
    }

    get toggleAIElement(): HTMLElement {
        return this._toggleAIElement;
    }

    set toggleAIElement(_toggleAIElement: HTMLElement) {
        this._toggleAIElement = _toggleAIElement;
    }

    get AILevelElement(): HTMLElement {
        return this._AILevelElement;
    }

    set AILevelElement(_AILevelElement: HTMLElement) {
        this._AILevelElement = _AILevelElement;
    }

    get toggleAIStartingElement(): HTMLElement {
        return this._toggleAIStartingElement;
    }

    set toggleAIStartingElement(_toggleAIStartingElement: HTMLElement) {
        this._toggleAIStartingElement = _toggleAIStartingElement;
    }

    get keyboardBindingsElement(): HTMLElement {
        return this._keyboardBindingsElement;
    }

    set keyboardBindingsElement(_keyboardBindingsElement: HTMLElement) {
        this._keyboardBindingsElement = _keyboardBindingsElement;
    }

}