import { CELL_MARGIN, NUMBER_OF_COLUMNS, NUMBER_OF_ROWS } from "../const";


export class BaseView {


    app: HTMLElement;

    constructor() {
        this.app = document.getElementById("main");
    }

    // Create an element with an optional CSS class
    public createElement(tag: string, classNames: string[]): HTMLElement {
        const element: HTMLElement = document.createElement(tag)
        if (classNames && classNames.length !== 0) {
            classNames.forEach((className: string) => element.classList.add(className));
        }
        return element
    }

    public createInputElement(type: string, defaultValue: string, classNames: string[]): HTMLInputElement {
        const element: HTMLInputElement = document.createElement("input");
        if (classNames && classNames.length !== 0) {
            classNames.forEach((className: string) => element.classList.add(className));
        }
        element.type = type;
        element.value = defaultValue;
        return element;
    }

    public createCustomableInputElement(type: string, defaultValue: string, classNames: string[], inputClassNames: string[]): HTMLElement {
        const element: HTMLElement = document.createElement("div");
        if (classNames && classNames.length !== 0) {
            classNames.forEach((className: string) => element.classList.add(className));
        }
        const inputElement: HTMLInputElement = this.createInputElement(type, defaultValue, ["hidden", ...inputClassNames]);
        element.appendChild(inputElement);
        return element;
    }

    // Retrieve an element from the DOM
    public getElement(selector): HTMLElement {
        const element = document.querySelector(selector)

        return element
    }

    public convertPXToVH(px: number): number {
        return px * (100 / document.documentElement.clientHeight);
    }

    public RGBStringToRGBArray(color: string): string[] {
        return color.substring(4, color.length - 1)
            .replace(/ /g, '')
            .split(',');
    }

    public hexToRgb(hex: string) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex.trim());
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    public getTokenSize(): { width: string, height: string } {
        const gridElement = document.querySelector(".grid");
        const gridStyle = getComputedStyle(gridElement);
        const gridWidth = parseFloat(gridStyle.width);
        const gridHeight = parseFloat(gridStyle.height);
        const width = String(this.convertPXToVH((gridWidth / NUMBER_OF_COLUMNS) - (gridWidth / NUMBER_OF_COLUMNS) * CELL_MARGIN * 2)) + "vh";
        const height = String(this.convertPXToVH((gridHeight / NUMBER_OF_ROWS) - (gridHeight / NUMBER_OF_ROWS) * CELL_MARGIN * 2)) + "vh";
        return { width, height };
    }

    public getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    public get docStyle() : CSSStyleDeclaration {
        return getComputedStyle(document.documentElement);
    }

    public cssVar(varName : string) {
        return this.docStyle.getPropertyValue(`--${varName}`);
    }

    public setCssVar(varName : string, value : string) {
        document.documentElement.style.setProperty(`--${varName}`, value);
    }
}