import { viewEvents } from "../Controller/Controller";
import { BaseView } from "./BaseView";
import { GridView } from "./GridView";


export class VictoryView extends BaseView {

    private _gridElement: HTMLElement;
    private _modal: HTMLElement;
    private _victoryText: HTMLElement;
    private _instructionsText: HTMLElement;
    private _isShuttingDown: boolean;
    private _gridView: GridView;
    private _restartFunction: (ev: KeyboardEvent) => void;

    constructor(winner: number, isWinnerAI: boolean, gridView: GridView) {
        super();
        this.gridView = gridView;
        this.isShuttingDown = false;
        this.gridElement = document.querySelector(".grid");
        this.modal = this.createElement("div", ["victory-modal", "neon-box", "player" + winner, "ignition"]);
        this.modal.addEventListener("animationend", (ev: AnimationEvent) => {
            if (ev.animationName === "ignition-flicker") {
                this.modal.classList.remove("ignition");
            }
        })
        this.victoryText = this.createElement("div", ["neon-text", "victory-text", "player" + winner]);
        this.victoryText.innerText = winner === -1 ? "Egalité" : (isWinnerAI ? "Défaite" : "Victoire");
        this.instructionsText = this.createElement("div", ["neon-text", "instructions-text", "player" + winner]);
        this.instructionsText.innerText = "Appuyez sur R pour rejouer";
        this.modal.appendChild(this.victoryText);
        this.modal.appendChild(this.instructionsText);
        this.gridElement.prepend(this.modal);
        this.restartFunction = this.onRestart.bind(this);
        window.addEventListener("keyup", this.restartFunction);
    }

    private onRestart(ev: KeyboardEvent) {
        if (ev.key === "r" || ev.key === "R" && !this.isShuttingDown) {
            this.isShuttingDown = true;
            this.instructionsText.classList.add("shutdown");
            this.victoryText.classList.add("shutdown");
            this.modal.classList.add("shutdown");
            const shutdownDuration = parseFloat(this.cssVar('shutdown-duration').slice(0, -1));
            window.removeEventListener("keyup", this.restartFunction);
            setTimeout(() => {
                viewEvents.emit("canQuickRestart");
                viewEvents.emit("quickRestart");
            }, shutdownDuration * 1000);
        }
    }


    get gridElement(): HTMLElement {
        return this._gridElement;
    }

    set gridElement(_gridElement: HTMLElement) {
        this._gridElement = _gridElement;
    }

    get modal(): HTMLElement {
        return this._modal;
    }

    set modal(_modal: HTMLElement) {
        this._modal = _modal;
    }

    get victoryText(): HTMLElement {
        return this._victoryText;
    }

    set victoryText(_victoryText: HTMLElement) {
        this._victoryText = _victoryText;
    }

    get instructionsText(): HTMLElement {
        return this._instructionsText;
    }

    set instructionsText(_instructionsText: HTMLElement) {
        this._instructionsText = _instructionsText;
    }

    get isShuttingDown(): boolean {
        return this._isShuttingDown;
    }

    set isShuttingDown(_isShuttingDown: boolean) {
        this._isShuttingDown = _isShuttingDown;
    }

    get gridView(): GridView {
        return this._gridView;
    }

    set gridView(_gridView: GridView) {
        this._gridView = _gridView;
    }


    get restartFunction() {
        return this._restartFunction;
    }

    set restartFunction(_restartFunction) {
        this._restartFunction = _restartFunction;
    }
}