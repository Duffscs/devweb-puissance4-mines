import { Model } from "../Model/Model";
import { GridView } from "../View/GridView";
import { UIView } from "../View/UIView";
import { EventEmitter } from 'events';

export const viewEvents = new EventEmitter();
export const modelEvents = new EventEmitter();
viewEvents.setMaxListeners(15);
modelEvents.setMaxListeners(15);

export class Controller {
    private model: Model;
    private view: GridView;
    private uiView: UIView;

    constructor() {
        this.model = new Model();
        this.view = new GridView();

        viewEvents.once("start", () => {
            viewEvents.on("helpPlayer", this.helpPlayer.bind(this));
            viewEvents.on("getPreview", this.getPreview.bind(this));
            viewEvents.on("nextTurn", this.nextTurn.bind(this));
            viewEvents.on("quickRestart", this.quickRestart.bind(this));
            viewEvents.on("toggleAI", this.toggleAI.bind(this));
            viewEvents.on("toggleAIStart", this.toggleAIStart.bind(this));
            viewEvents.on("changeAILevel", this.changeAILevel.bind(this));
            modelEvents.on("getPlayerNextMove", this.getPlayerNextMove.bind(this));
            modelEvents.on("endTurn", this.endTurn.bind(this));
            modelEvents.on("gameOver", this.gameOver.bind(this));
        });

        viewEvents.on("start", () => {
            this.model.reset();
            this.model.play();
        })
        this.uiView = new UIView(this.view);
    }

    private getPreview(column: number) {
        const row = this.model.previewMove(column);
        viewEvents.emit("preview", row);
    }

    private toggleAI() {
        this.model.toggleAI();
        if (this.view.canPlay)
            this.model.play();
        this.uiView.toggleAI();
        this.uiView.setAIStart(this.model.AI_FirstPlayer);
    }

    private async helpPlayer() {
        let move = await this.model.getAIHelp();
        this.view.hint(move);
    }

    private toggleAIStart() {
        this.model.toggleAI_FirstPlayer();
        this.model.play();
        this.uiView.setAITrue();
        this.uiView.toggleAIStart();
        // this.quickRestart();
    }

    private changeAILevel(level: number) {
        this.model.AI_Level = level;
        if (this.model.AI_On == false) {
            this.model.toggleAI();
            this.model.play();
        }
        this.uiView.setAITrue();
        this.uiView.changeAILevel(level);
    }


    private getPlayerNextMove() {
        console.log("getPlayerNextMove");

        viewEvents.once("click", (col: number) => {
            modelEvents.emit("nextMove", col);
        });
    }

    private quickRestart() {
        viewEvents.removeAllListeners("click");
        modelEvents.removeAllListeners("nextMove");
        this.model.reset();
        this.uiView.canRestart = false;
        this.view.reset();
        this.model.play();
    }

    private endTurn(column: number, row: number) {
        this.view.playAnimation(column, row)
    }

    private nextTurn() {
        this.model.play();
    }

    private gameOver() {
        const winningTokens = this.model.grid.getWinningTokens();
        const isWinnerAI = this.model.grid.isWinnerAI();
        this.view.gameOver(winningTokens, isWinnerAI);
        this.uiView.canRestart = false;
    }

    //#region Setters and getters

    public getModel(): Model {
        return this.model;
    }

    public setModel(newModel: Model): void {
        this.model = newModel;
    }

    public getView(): GridView {
        return this.view;
    }

    public setView(newView: GridView): void {
        this.view = newView;
    }

    //#endregion
}