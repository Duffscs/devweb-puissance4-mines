import { Controller } from './Controller/Controller';

// Création du controller après le chargement de la page complète
window.onload = () => {
    const controller = new Controller();
}