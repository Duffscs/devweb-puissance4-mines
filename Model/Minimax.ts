type GamePosition = any;
type Move = any;

export class Minimax {


    evaluate: (position: GamePosition) => number;
    getChilds: (position: GamePosition) => number[];
    isOver: (position: GamePosition) => boolean;
    play: (position: GamePosition, move: Move) => GamePosition;
    undoPlay: (position: GamePosition, move: Move) => GamePosition;

    constructor(evaluate: (position: GamePosition) => number, getChilds: (position: GamePosition) => Move[], isOver: (position: GamePosition) => boolean, play: (position: GamePosition, move: Move) => GamePosition, undoPlay: (position: GamePosition, move: Move) => GamePosition) {
        this.evaluate = evaluate;
        this.getChilds = getChilds;
        this.isOver = isOver;
        this.play = play;
        this.undoPlay = undoPlay;
    }

    public getBestPosition(maximizingPlayer: boolean, position: GamePosition, depth : number): number {
        let bestMove = 0;
        let minMaxEval = maximizingPlayer ? -Infinity : Infinity;
        for (let child of this.getChilds(position)) {
            position = this.play(position, child);
            const val = this.minimax(position, depth, -Infinity, Infinity, !maximizingPlayer);
            if (val > minMaxEval && maximizingPlayer) {
                minMaxEval = val;
                bestMove = child;
            } else if (val < minMaxEval && !maximizingPlayer) {
                minMaxEval = val;
                bestMove = child;
            }
            position = this.undoPlay(position, child);
        }

        return bestMove;
    }


    private minimax(position: GamePosition, depth: number, alpha: number, beta: number, maximizingPlayer: boolean) {

        if (depth == 0 || this.isOver(position)) {
            return this.evaluate(position);
        }

        let compare = maximizingPlayer ? Math.max : Math.min;
        let minMaxEval = maximizingPlayer ? -Infinity : Infinity;
        for (let child of this.getChilds(position)) {
            position = this.play(position, child);
            const val = this.minimax(position, depth - 1, alpha, beta, !maximizingPlayer);
            minMaxEval = compare(minMaxEval, val);
            if (maximizingPlayer) alpha = Math.max(alpha, minMaxEval);
            else beta = Math.min(beta, minMaxEval);
            position = this.undoPlay(position, child);
            if (beta <= alpha) break;
        }
        return minMaxEval;
    }
}