import { NUMBER_OF_COLUMNS, NUMBER_OF_ROWS, EMPTY_POSITION } from "../const";
import { AI } from "./AI";
import { GridIterator, Diagonal, Line, Row, Column, Position, All } from "./GridIterator";
import { IPlayer } from "./IPlayer";
import { Player } from "./Player";

export class Grid {

    private layout: string[][];
    private _turn: number;

    private _playerOne: IPlayer;
    private _playerTwo: IPlayer;
    private _currentPlayer: IPlayer;
    private _disabled: boolean;

    constructor(playerOne: IPlayer, playerTwo: IPlayer) {
        this.layout = this.initLayout();
        this.turn = 0;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.currentPlayer = this.playerOne;
    }

    private initLayout(): string[][] {
        const emptyColumn = new Array(this.columnNumbers).fill(EMPTY_POSITION + "");
        return Array(this.rowNumbers).fill('').map(() => [...emptyColumn]);
    }

    public log(): void {
        let string = this.layout.map((line: string[]) => line.join(' ')).join('\n');
        console.log(string);
    }

    public async play() {
        let col: number, row: number;
        let currentPlayer = this.currentPlayer;
        do {
            col = await currentPlayer.nextMove(this);
            row = this.addToken(col);
        } while (row == -1);
        return [col, row];
    }

    public addToken(columnNum: number): number {
        let rowNum: number;

        for (let { row, rowNum: lineNum } of this.iterateByRows()) {
            if (row[columnNum] == EMPTY_POSITION)
                rowNum = lineNum;
        }

        if (rowNum == null)
            return -1;

        this.layout[rowNum][columnNum] = this.currentPlayer.token;
        this.currentPlayer = this.nextPlayer();
        this.turn++;
        return rowNum;
    }

    public removeToken(colNum: number) {
        let end = false;
        for (let { row } of this.iterateByRows()) {
            if (row[colNum] != EMPTY_POSITION && !end) {
                row[colNum] = EMPTY_POSITION;
                end = true;
            }
        }
        this.currentPlayer = this.nextPlayer();
        this.turn--;
    }

    private nextPlayer(): IPlayer {
        return this.currentPlayer == this.playerOne ? this.playerTwo : this.playerOne;
    }

    public isGameOver() {

        for (let { row, column, diagonal } of this.iterateAll()) {
            if (this.isConnectFour(row?.row) || this.isConnectFour(column?.column) || this.isConnectFour(diagonal?.diagonal)) {
                return true;
            }
        }

        return this.isFull();
    }

    private isFull(): boolean {
        let full = true;
        for (const { row } of this.iterateByRows()) {
            if (this.lineIsFull(row) && full) {
                full = false;
            }
        }
        return full;
    }

    private lineIsFull(line: string[]): boolean {
        return line.every(token => token !== '');
    }

    private isConnectFour(line: string[]): boolean {
        if (line == null) return false;
        const stringLine = line.join('');
        const regex = new RegExp(`(${this.playerTwo.token}{4})|(${this.playerOne.token}{4})`);
        return regex.test(stringLine);
    }

    public childsPosition(): number[] {
        const childs: number[] = [];
        for (let i = 0; i < this.columnNumbers; i++) {
            if (this.addToken(i) != -1) {
                childs.push(i);
                this.removeToken(i);
            }
        }
        return childs;
    }

    public static gridFromDeserialized(serialized: any): Grid {
        serialized._playerOne.token = serialized._playerOne.token;
        serialized._playerTwo.token = serialized._playerTwo.token;
        let player1 = Player.fromDeserialized(serialized._playerOne);
        let player2 = Player.fromDeserialized(serialized._playerTwo);
        let grid = new Grid(player1, player2);
        grid._currentPlayer = serialized._currentPlayer._token == player1.token ? player1 : player2;
        grid._turn = serialized._turn;
        grid.layout = serialized.layout;
        return grid;
    }

    public isWinnerAI(): boolean {
        return this.nextPlayer() instanceof AI;
    }


    //#region getLine and Token

    public getToken(rowNum: number, colNum: number): string {
        return this.layout[rowNum][colNum];
    }

    public getRow(rowNum: number): Line {
        return this.layout[rowNum];
    }

    public getColumn(columnNum: number): Line {
        const column = [];
        for (let lineNum = 0; lineNum < this.rowNumbers; lineNum++) {
            column.push(this.getToken(lineNum, columnNum));
        }
        return column;
    }

    /**
     * Renvoie un tableau de toute les positions du plateau qui sont sur la diagonale de pente -1
     * en partant de la position (rowNum, columnNum)
     * La position peut être hors du plateau de jeu 
     */
    public getDiagonalRightDown(rowNum: number, columnNum: number): Line {
        const diagonal = [];
        [rowNum, columnNum] = this.refocusPosDiagonalRightDown(rowNum, columnNum);

        while (rowNum < this.rowNumbers && columnNum < this.columnNumbers) {
            diagonal.push(this.getToken(rowNum, columnNum));
            rowNum++;
            columnNum++;
        }
        return diagonal;
    }

    private refocusPosDiagonalRightDown(rowNum: number, columnNum: number) {
        if (columnNum < rowNum) {
            rowNum = rowNum - columnNum;
            columnNum = 0;
        } else {
            columnNum = columnNum - rowNum;
            rowNum = 0;
        }
        return [rowNum, columnNum];
    }

    /**
     * Renvoie un tableau de toute les positions du plateau qui sont sur la diagonale de pente 1
     * en partant de la position (rowNum, columnNum)
     * La position peut être hors du plateau de jeu 
     */
    public getDiagonalLeftDown(rowNum: number, columnNum: number): Line {
        const diagonal: string[] = [];
        [rowNum, columnNum] = this.refocusPosDiagonalLeftDown(rowNum, columnNum);

        while (columnNum >= 0 && rowNum < this.rowNumbers) {
            diagonal.push(this.getToken(rowNum, columnNum));
            rowNum++;
            columnNum--;
        }
        return diagonal;
    }

    private refocusPosDiagonalLeftDown(rowNum: number, columnNum: number) {
        if (rowNum + columnNum < this.columnNumbers) {
            columnNum = rowNum + columnNum;
            rowNum = 0;
        } else {
            rowNum = rowNum - this.rowNumbers + columnNum;
            columnNum = this.columnNumbers - 1;
        }
        return [rowNum, columnNum];
    }

    //#endregion

    //#region GetWinningTokens

    public getWinningTokens() {
        let winningTokens: { row: number, column: number }[];
        for (let { row, column, diagonal } of this.iterateAll()) {

            const line = row?.row ?? column?.column ?? diagonal?.diagonal;

            const [start, end] = this.getWinningTokensLineStartEndPos(line);
            if (diagonal) {
                winningTokens = this.getWinningTokensDiagonal(start, end, diagonal.isRightDown, diagonal.diagonalNum);
            } else {
                winningTokens = this.getWinningTokensLine(start, end, row?.rowNum, column?.columnNum);
            }

            if (winningTokens)
                return winningTokens;

        }

        return [];
    }

    private getWinningTokensDiagonal(start: number, end: number, isRightDown: boolean, diagonalNum: number): { row: number, column: number }[] | undefined {
        if (start == end) return;

        const winningTokens = [];

        if (isRightDown) {
            let [row, col] = this.refocusPosDiagonalRightDown(0, diagonalNum);
            for (let i = start; i < end; i++) {
                winningTokens.push({ row: row + i, column: col + i });
            }

        } else {
            let [row, col] = this.refocusPosDiagonalLeftDown(0, diagonalNum);
            for (let i = start; i < end; i++) {
                winningTokens.push({ row: row + i, column: col - i });
            }
        }

        return winningTokens;
    }

    private getWinningTokensLine(start: number, end: number, row: number | undefined, col: number | undefined): { row: number, column: number }[] | undefined {
        if (start == end) return;
        const winningTokens = [];
        for (let i = start; i < end; i++) {
            winningTokens.push({ row: row ?? i, column: col ?? i });
        }
        return winningTokens;
    }

    private getWinningTokensLineStartEndPos(line: Line) {
        if (line == null)
            return;

        const lineStr = line.join('');
        const token = this.nextPlayer().token;
        for (let i = line.length; i >= 4; i--) {
            const sub = x(token, i);
            let startIndex = lineStr.indexOf(sub);
            if (startIndex != -1)
                return [startIndex, startIndex + i];
        }

        return [0, 0];
    }

    //#endregion

    //#region Iterators

    iterateByRows: () => IterableIterator<Row> = GridIterator.iterateByRows.bind(this);
    iterateByColumns: () => IterableIterator<Column> = GridIterator.iterateByColumns.bind(this);
    iterateByRowColum: () => IterableIterator<Position> = GridIterator.iterateByRowColum.bind(this);
    iterateByColumnRow: () => IterableIterator<Position> = GridIterator.iterateByColumnRow.bind(this);
    iterateByDiagonalRightDown: () => IterableIterator<Diagonal> = GridIterator.iterateByDiagonalRightDown.bind(this);
    iterateByDiagonalLeftDown: () => IterableIterator<Diagonal> = GridIterator.iterateByDiagonalLeftDown.bind(this);
    iterateAll: () => IterableIterator<All> = GridIterator.iterateAll.bind(this);

    //#endregion

    //#region Getters and Setters

    get rowNumbers(): number {
        return NUMBER_OF_ROWS;
    }

    get columnNumbers() {
        return NUMBER_OF_COLUMNS;
    }

    get turn(): number {
        return this._turn;
    }

    set turn(turn: number) {
        this._turn = turn;
    }

    get playerOne(): IPlayer {
        return this._playerOne;
    }

    set playerOne(playerOne: IPlayer) {
        this._playerOne = playerOne;
    }

    set playerTwo(playerTwo: IPlayer) {
        this._playerTwo = playerTwo;
    }

    get playerTwo(): IPlayer {
        return this._playerTwo;
    }

    get currentPlayer(): IPlayer {
        return this._currentPlayer;
    }

    set currentPlayer(currentPlayer: IPlayer) {
        this._currentPlayer = currentPlayer;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    get disabled(): boolean {
        return this._disabled;
    }

    //#endregion

}

// x('a', 3) => 'aaa'
function x(string: string, loop: number) {
    let result = '';
    for (let i = 0; i < loop; i++) {
        result += string;
    }
    return result;
}