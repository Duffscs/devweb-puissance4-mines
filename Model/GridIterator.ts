import 'regenerator-runtime/runtime'; // Nécessaire pour que les fonctions génératrices fonctionnent
import { Grid } from "./Grid";

export type Line = string[];
export type Position = { token: string, columnNum: number, rowNum: number };
export type Row = { rowNum: number, row: Line };
export type Column = { columnNum: number, column: Line };
export type Diagonal = { diagonalNum: number, diagonal: Line, isRightDown: boolean };
export type All = { row: Row, column: Column, diagonal: Diagonal };

export class GridIterator {

    public static *iterateByRows(this: Grid): IterableIterator<Row> {
        for (let rowNum = 0; rowNum < this.rowNumbers; rowNum++) {
            yield {
                row: this.getRow(rowNum),
                rowNum
            };
        }
    }

    public static *iterateByColumns(this: Grid): IterableIterator<Column> {
        for (let columnNum = 0; columnNum < this.columnNumbers; columnNum++) {
            yield {
                column: this.getColumn(columnNum),
                columnNum
            };
        }
    }

    public static *iterateByRowColum(this: Grid): IterableIterator<Position> {
        for (let rowNum = 0; rowNum < this.rowNumbers; rowNum++) {
            for (let columnNum = 0; columnNum < this.columnNumbers; columnNum++) {
                yield {
                    token: this.getToken(rowNum, columnNum),
                    rowNum,
                    columnNum
                };
            }
        }
    }

    public static *iterateByColumnRow(this: Grid): IterableIterator<Position> {
        for (let columnNum = 0; columnNum < this.columnNumbers; columnNum++) {
            for (let rowNum = 0; rowNum < this.rowNumbers; rowNum++) {
                yield {
                    token: this.getToken(rowNum, columnNum),
                    rowNum,
                    columnNum
                };
            }
        }
    }

    public static *iterateByDiagonalRightDown(this: Grid): IterableIterator<Diagonal> {
        let totalNumberOfDiagonals = this.rowNumbers + this.columnNumbers - 1;
        for (let index = 0; index < totalNumberOfDiagonals; index++) {
            let diagonalNum = index - (this.rowNumbers - 1);
            yield {
                diagonal: this.getDiagonalRightDown(0, diagonalNum),
                diagonalNum,
                isRightDown: true
            };
        }
    }

    public static *iterateByDiagonalLeftDown(this: Grid): IterableIterator<Diagonal> {
        let totalNumberOfDiagonals = this.rowNumbers + this.columnNumbers - 1;
        for (let diagonalNum = 0; diagonalNum < totalNumberOfDiagonals; diagonalNum++) {
            yield {
                diagonal: this.getDiagonalLeftDown(0, diagonalNum),
                diagonalNum,
                isRightDown: false
            };
        }
    }

    public static *iterateAll(this: Grid): IterableIterator<All> {
        for (const row of this.iterateByRows()) {
            yield { row, column: null, diagonal: null };
        }

        for (const column of this.iterateByColumns()) {
            yield { row: null, column, diagonal: null };
        }

        for (const diagonal of this.iterateByDiagonalRightDown()) {
            yield { row: null, column: null, diagonal };
        }

        for (const diagonal of this.iterateByDiagonalLeftDown()) {
            yield { row: null, column: null, diagonal };
        }

    }
}