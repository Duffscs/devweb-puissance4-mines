import { Grid } from "./Grid";
import { Line } from "./GridIterator";

export class GridEvaluator {
    p1Token: string;
    p2Token: string;

    constructor() { }


    public evaluate(grid: Grid): number {
        this.p1Token = grid.playerOne.token;
        this.p2Token = grid.playerTwo.token;
        let score = 0;

        // Evaluate by rows, columns, diagonals
        for (const { row, column, diagonal } of grid.iterateAll()) {
            const line = row?.row ?? column?.column ?? diagonal?.diagonal;
            score += this.evaluateLine(line);
        }

        return score;
    }

    private evaluateLine(line: Line): number {
        let score = 0;
        const lineTokens = line.join('');
        score += this.evaluateLinePlayer(lineTokens, this.p1Token, this.p2Token);
        score -= this.evaluateLinePlayer(lineTokens, this.p2Token, this.p1Token);
        return score;
    }

    private evaluateLinePlayer(line: string, token: string, enemyToken: string): number {
        if (line.length < 4)
            return 0;

        const both = `(${token}|${enemyToken})`
        const surrounded = (nb: number) => new RegExp(`(^${token}{${nb}}${both})|(${both}${token}{${nb}}${both})`).test(line);

        // Check for 4 in a row
        if (line.includes(x(token, 4))) {
            return 10000;
        }

        // Check surrounded 3 token
        if (surrounded(3)) {
            return 0;
        } else if (line.includes(x(token, 3))) { // Check for 3 in a row
            return 100;
        }

        // Check for 2 in a row
        if (!surrounded(2) && line.includes(x(token, 2))) {
            return 10;
        }

        // Check for 1 in a row
        if (!surrounded(1) && line.includes(token)) {
            return 1;
        }

        return 0;
    }
}

// x('a', 3) => 'aaa'
function x(string: string, loop: number) {
    let result = '';
    for (let i = 0; i < loop; i++) {
        result += string;
    }
    return result;
}