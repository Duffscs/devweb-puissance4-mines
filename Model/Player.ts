import { modelEvents } from "../Controller/Controller";
import { Grid } from "./Grid";
import { IPlayer } from "./IPlayer";

export class Player implements IPlayer {

    private _token: string;
    private _canPlay: boolean;

    constructor(token: string) {
        this.token = token;
        this.canPlay = true;
    }

    public nextMove(grid: Grid): Promise<number> | number {
        const nextMove: Promise<number> = new Promise((resolve, reject) =>
            modelEvents.once("nextMove", (move: number) => {
                if (this.canPlay) {
                    return resolve(move);
                }
                reject("Player cannot play");
            })
        );

        modelEvents.emit("getPlayerNextMove");
        return nextMove;
    }

    public static fromDeserialized(serialized): Player {
        return new Player(serialized._token);
    }

    //#region Getters & Setters

    public get token(): string {
        return this._token;
    }

    public set token(token: string) {
        this._token = token;
    }

    public get canPlay(): boolean {
        return this._canPlay;
    }

    public set canPlay(canPlay: boolean) {
        this._canPlay = canPlay;
    }


    //#endregion

}