import { Grid } from "./Grid";
import { GridEvaluator } from "./GridEvaluator";
import { Minimax } from "./Minimax";

const getChilds = (grid: Grid) => grid.childsPosition();
const evaluate = (grid: Grid) => new GridEvaluator().evaluate(grid);
const isGameOver = (grid: Grid) => grid.isGameOver();
const play = (grid: Grid, move: number) => {
    grid.addToken(move);
    return grid;
}
const undoPlay = (grid: Grid, move: number) => {
    grid.removeToken(move);
    return grid;
}

onmessage = (e) => {
    let grid = Grid.gridFromDeserialized(JSON.parse(e.data.grid));
    let level: number = e.data.level;
    let minimax = new Minimax(evaluate, getChilds, isGameOver, play, undoPlay);
    let result = minimax.getBestPosition(grid.turn % 2 === 0, grid, level);
    postMessage(result);
}