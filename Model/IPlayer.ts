import { Grid } from "./Grid";

export interface IPlayer {
    nextMove: (grid: Grid) => Promise<number> | number;
    //#region Getters & Setters
    get token(): string;
    set token(token: string);
    set canPlay(canPlay: boolean);
    //#endregion
}