import { MAX_AI_LEVEL, PLAYER_ONE_TOKEN, PLAYER_TWO_TOKEN } from '../const';
import { AI } from './AI';
import { Grid } from './Grid';
import { IPlayer } from './IPlayer';
import { Player } from './Player';
import { modelEvents } from "../Controller/Controller";


export class Model {

    private _grid: Grid;
    private _AI_On: boolean;
    private _AI_FirstPlayer: boolean;
    private _AI_Level: number;

    constructor() {
        this.AI_On = false;
        this.AI_FirstPlayer = false;
        this.AI_Level = 1;
    }

    private initGrid() {
        let [p1, p2] = this.initPlayer();
        this.grid = new Grid(p1, p2);
    }

    public reset() {
        if (this?.grid) {
            this.grid.playerOne.canPlay = false;
            this.grid.playerTwo.canPlay = false;
        }
        this.initGrid();
    }

    private initPlayer() {
        let playerOne: IPlayer;
        let playerTwo: IPlayer;
        if (this.AI_On) {
            if (this.AI_FirstPlayer) {
                playerOne = new AI(PLAYER_ONE_TOKEN, this.AI_Level);
                playerTwo = new Player(PLAYER_TWO_TOKEN);
            } else {
                playerOne = new Player(PLAYER_ONE_TOKEN);
                playerTwo = new AI(PLAYER_TWO_TOKEN, this.AI_Level);
            }
        } else {
            playerOne = new Player(PLAYER_ONE_TOKEN);
            playerTwo = new Player(PLAYER_TWO_TOKEN);
        }
        return [playerOne, playerTwo];
    }


    public play() {
        if (this.grid.isGameOver()) {
            modelEvents.emit('gameOver');
        }
        let currentPlayer = this.grid.currentPlayer;
        let currentGrid = this.grid;
        this.grid.play().then(([col, row]) => {
            this.grid.log();
            if (!currentGrid.disabled) {
                modelEvents.emit("endTurn", col, row);
            }
        }).catch(() => {
            console.log(`Player turn ${currentPlayer.token} passed`);
        });
    }

    public toggleAI() {

        this.AI_On = !this.AI_On;
        this.grid.playerOne.canPlay = false;
        this.grid.playerTwo.canPlay = false;
        if (this.grid.turn % 2 == 0) {
            this.AI_FirstPlayer = false;
        } else {
            this.AI_FirstPlayer = true;
        }
        const [p1, p2] = this.initPlayer();
        this.grid.playerOne = p1;
        this.grid.playerTwo = p2;

        if (this.grid.turn % 2 == 0) {
            this.grid.currentPlayer = p1;
        } else {
            this.grid.currentPlayer = p2;
        }

        if (this.AI_On == false) {
            this.AI_FirstPlayer = false;
        }
    }

    public toggleAI_FirstPlayer() {
        this.AI_FirstPlayer = !this.AI_FirstPlayer;
        this.AI_On = true;
        this.grid.playerOne.canPlay = false;
        this.grid.playerTwo.canPlay = false;

        const [p1, p2] = this.initPlayer();
        this.grid.playerOne = p1;
        this.grid.playerTwo = p2;
        if (this.grid.turn % 2 == 0) {
            this.grid.currentPlayer = p1;
        } else {
            this.grid.currentPlayer = p2;
        }
    }

    public previewMove(column: number) {
        let row = this.grid.addToken(column);
        if (row !== -1) {
            this.grid.removeToken(column);
        }
        return row;
    }

    public getAIHelp(): Promise<number> {
        if (this.grid.currentPlayer instanceof AI)
            return new Promise(resolve => resolve(-1));

        let ai = new AI(this.grid.currentPlayer.token, MAX_AI_LEVEL);
        return ai.nextMove(this.grid);
    }

    //#region Getters & Setters

    public get grid(): Grid {
        return this._grid;
    }

    public set grid(grid: Grid) {
        this._grid = grid;
    }

    public get AI_On(): boolean {
        return this._AI_On;
    }

    public set AI_On(AI_On: boolean) {
        this._AI_On = AI_On;
    }

    public get AI_FirstPlayer(): boolean {
        return this._AI_FirstPlayer;
    }

    public set AI_FirstPlayer(AI_FirstPlayer: boolean) {
        this._AI_FirstPlayer = AI_FirstPlayer;
    }

    public get AI_Level(): number {
        return this._AI_Level;
    }


    public set AI_Level(level: number) {
        this._AI_Level = level;
    }


    //#endregion
}