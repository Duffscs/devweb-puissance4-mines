import { Grid } from "./Grid";
import { IPlayer } from "./IPlayer";

export class AI implements IPlayer {

    private _token: string;
    private workerThread: Worker;
    private level: number;
    private _canPlay: boolean;

    constructor(token: string, level: number) {
        this.token = token;
        this.workerThread = new Worker('./Worker.ts');
        this.level = level;
        this.canPlay = true;
    }

    public nextMove(grid: Grid): Promise<number> {
        this.workerThread.postMessage({ grid: JSON.stringify(grid), level: this.level });
        return new Promise((resolve, reject) => {
            this.workerThread.onmessage = (e) => {
                if (this.canPlay) {
                    return resolve(e.data)
                }
                reject("Player cannot play");
            };
        });
    }

    //#region Getters & Setters

    public get token(): string {
        return this._token;
    }

    public set token(token: string) {
        this._token = token;
    }

    public get canPlay(): boolean {
        return this._canPlay;
    }

    public set canPlay(canPlay: boolean) {
        this._canPlay = canPlay;
    }

    //#endregion
}